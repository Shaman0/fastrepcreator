class SFSClient {
		constructor(config, clConf) { // Create SmartFox client instance
		this.clientConfig = clConf;
		// Create SmartFox client instance
		this.sfs = new SmartFox(config);
		this.room = config.room;
		// Set client details
		var platform = navigator.appName;
		var version = navigator.appVersion;
		this.sfs.setClientDetails(platform, version);

		// Add event listeners
		this.sfs.addEventListener(SFS2X.SFSEvent.CONNECTION, this.onConnection, this);
		this.sfs.addEventListener(SFS2X.SFSEvent.CONNECTION_LOST, this.onConnectionLost, this);
		this.sfs.addEventListener(SFS2X.SFSEvent.PING_PONG, this.onPingPong, this);
		this.sfs.addEventListener(SFS2X.SFSEvent.ROOM_JOIN_ERROR, this.onRoomJoinError, this);
		this.sfs.addEventListener(SFS2X.SFSEvent.ROOM_JOIN, this.onRoomJoin, this);
		this.sfs.addEventListener(SFS2X.SFSEvent.LOGIN, this.onLogin, this);
		//this.sfs.addEventListener(SFS2X.SFSEvent.PRIVATE_MESSAGE, this.onPrivateMessage, this);
		
		this.sfs.connect();
}

onConnection(event)
{
	if (event.success)
	{
		console.log("Connected to SmartFoxServer 2X!");

		// Perform login
		var uName = this.clientConfig.login;
		var isSent = this.sfs.send(new SFS2X.Requests.System.LoginRequest(uName));
		console.log(uName);
	}
	else
	{
		console.log("Connection failed: " + (event.errorMessage ? event.errorMessage + " (" + event.errorCode + ")" : "Невозможно подключиться к серверу?"), false);
		errorMessageShow(event.errorMessage ? event.errorMessage + " (" + event.errorCode + ")" : "Is the server running at all?");
	}
}
onLogin(event)
{	
	var rooms = this.sfs.roomManager.getRoomList();
		for (var i=0; i<rooms.length; i++) {
			if (rooms[i].name == this.clientConfig.room) {
				this.sfs.send(new SFS2X.Requests.System.JoinRoomRequest(rooms[i].id));
				this.room_id = rooms[i].id;
			}
		}
}
onConnectionLost(event)
{
	console.log('sfs Connection lost!');
	this.sfs.connect();
}
onRoomJoin(event)
{
	console.log('Room joined ' + event.room.name)
}
onRoomJoinError(event)
{
	console.log("Room join error: " + event.errorMessage + " (" + event.errorCode + ")", true);
}
sfs_return() {
	return this.sfs
}
users() {
	var userList = ['none']
	var room = this.sfs.getRoomById(this.room_id);
	if ( room != undefined) {
		userList = this.sfs.getRoomById(this.room_id).getUserList();
		for (var i=0;i<userList.length;i++)
			userList[i] = userList[i].name;
	}
	return userList
}
onPingPong(event)
{
	console.log('ping');
	var avgLag = Math.round(event.lagValue * 100) / 100;
	$("#lagLb").text("Average lag: " + avgLag + "ms");
	this.sfs.send(new SFS2X.Requests.System.ObjectMessageRequest(pingObject));
}
sendPrivateMessage(recepient,msg,params) {
		var user_id = sfs.userManager.getUserByName(recepient).id;
		sfs.send(new SFS2X.Requests.System.PrivateMessageRequest(msg, parseInt(user_id), params));
}

}