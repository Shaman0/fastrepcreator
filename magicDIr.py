import os
from shutil import copyfile

name = raw_input("Please input preferable name for standart files (.html, .css, .js) and directory\n");
options = raw_input("Please input options: 0: JQuery 1: SFS2X_API_JS.js, 2: sfsLogic.js\n");

if not os.path.exists('../' + name + "/css"):
    os.makedirs('../' + name + "/css")

if not os.path.exists('../' + name + "/js"):
    os.makedirs('../' + name + "/js")

if not os.path.exists('../' + name + "/images"):
    os.makedirs('../' + name + "/images")

if not os.path.exists('../' + name + "/libs"):
    os.makedirs('../' + name + "/libs")

if not os.path.exists('../' + name + "/css/fonts"):
    os.makedirs('../' + name + "/css/fonts")


f = open('../' + name + '/' + name + ".html", 'w');
f.write('<!DOCTYPE html>\n');
f.write('<html>\n<head>\n');
f.write('<meta charset="UTF-8">\n<link rel="stylesheet" type="text/css" href="css/' + name + '.css">\n');
f.write('<link rel="stylesheet" type="text/css" href="css/shared.css">\n');
f.write('<script type="text/javascript" src="js/' + name + '.js"></script>\n');
f.write('</head>\n<body>\n');
f.write('<div>Hello World!</div>\n');
f.write('</body>\n</html>')
f.close();

#############################
####### COPY FONTS ##########
#############################

src = 'css/fonts'
dest = ('../' + name + '/css/fonts/')

src_files = os.listdir(src)
for file_name in src_files:
    full_file_name = os.path.join(src, file_name)
    if (os.path.isfile(full_file_name)):
    	full_target_name = os.path.join(dest, file_name)
        copyfile(full_file_name, full_target_name)

#############################
###### COPY FONTS END #######
####### COPY JS START #######
#############################
dest = ('../' + name + '/libs/')

for option in options:
    option = int(option);
    if(option == 0):
        full_target_name = os.path.join(dest, "jquery-3.2.1.min.js")
        copyfile('js/jquery-3.2.1.min.js', full_target_name)
    if(option == 1):
        full_target_name = os.path.join(dest, "SFS2X_API_JS.js")
        copyfile('js/SFS2X_API_JS.js', full_target_name)
    if(option == 2):
        full_target_name = os.path.join(dest, "sfsLogic.js")
        copyfile('js/sfsLogic.js', full_target_name)

#############################
####### COPY JS END #########
#############################

copyfile('css/shared.css', '../' + name + '/css/shared.css');

open('../' + name + '/' + "js/" + name + ".js", 'w').close();
open('../' + name + '/' + "css/" + name + ".css", 'w').close();

s = name + ' directory and all standard files are created successfully'
print s
raw_input()